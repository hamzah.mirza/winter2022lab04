import java.util.Scanner;
public class Shop{
  public static void main (String[]args){
    Scanner reader = new Scanner(System.in);
    
    Pets[] animals = new Pets[4];
    for (int i = 0; i < 4; i++){
      
      System.out.println("Enter the name of the pet");
      String name = reader.next();
      System.out.println("Enter the classification of the pet (mammal? reptile? etc)");
      String classification = reader.next();
      System.out.println("Enter the lifespan of the pet");
      int lifespan = reader.nextInt();
      animals[i] = new Pets(name, classification, lifespan);
      animals[i].getName(); 
      animals[i].getClassification();
      animals[i].getLifespan();
    }
    
    System.out.println(animals[3].getName());
    System.out.println(animals[3].getClassification());
    System.out.println(animals[3].getLifespan());
    
    System.out.println("Above are the values before they get changed");
    
    System.out.println("Enter the new name of the pet");
    String newName = reader.next();
    System.out.println("Enter the classification of the pet (mammal? reptile? etc)");
    String newClassification = reader.next();
    System.out.println("Enter the lifespan of the pet");
    int newLifespan = reader.nextInt();
    
    animals[3].setName(newName);
    animals[3].setClassification(newClassification);
    animals[3].setLifespan(newLifespan);
    
    System.out.println(animals[3].getName());
    System.out.println(animals[3].getClassification());
    System.out.println(animals[3].getLifespan());
    
    animals[3].coolness();
  }
}
