public class Pets{
  public Pets(String name, String classification, int lifespan){
    this.name = name;
    this.classification = classification;
    this.lifespan = lifespan; 
  }
  private String name;
  private String classification;
  private int lifespan;
  
  public String getName(){
    return this.name;
  }
  public String getClassification(){
    return this.classification;
  }
  public int getLifespan(){
    return this.lifespan;
  }
  
  public void setName(String newName){
    this.name = newName;
  }
  public void setClassification(String newClassification){
    this.classification = newClassification;
  }
  public void setLifespan(int newLifespan){
    this.lifespan = newLifespan;
  }
  
  public void coolness (){
    if (this.name.equals("turtle")){
      System.out.println ("Awesome! You chose a cool pet");
    }else{
      System.out.println("Ehh. You chose a boring pet"); 
    }
  }
}
